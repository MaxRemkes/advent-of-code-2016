#!/usr/bin/python

import collections
import operator
import sys

lettersTotal = []

with open('input.txt') as f:
    lines = f.readlines()

    lineLength = len(lines[0])
    for x in range(0, lineLength - 1):
        lettersTotal.append(dict())

    for line in lines:
        line = line.rstrip('\n')

        ix = 0
        for c in line:
            lettersRow = lettersTotal[ix]

            if (c in lettersRow):
                lettersRow[c] += 1
            else:
                lettersRow[c] = 1

            ix += 1

for lettersRow in lettersTotal:
    lettersRow = collections.OrderedDict(sorted(lettersRow.items(), key=operator.itemgetter(1)))
    sys.stdout.write(lettersRow.items()[0][0])

sys.stdout.write('\n')
sys.stdout.flush()