<?php

require_once('Line.php');

$input = file_get_contents('data.txt');
$lines = explode("\r\n", $input);

$sum = 0;

foreach ($lines as $line) {

    $lineObj = new Line($line);
    $lineObj->extractData();
    //$lineObj->log();

    if ($lineObj->isValid()) {
        // echo '--- valid ---' . PHP_EOL;
        $sum += $lineObj->value();
    }
    else {
        // echo '--- NOT valid ---' . PHP_EOL;
    }
}

echo 'sum: ' . $sum . PHP_EOL;