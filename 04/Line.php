<?php

class Line {

    protected $data;
    protected $matchesNumbers;
    protected $matchesChars;
    protected $checkSum;
    protected $value;
    protected $charInfo;
    protected $allChars;
    protected $string;

    function __construct($data) {
        $this->data = $data;
    }

    public function extractData() {
        preg_match_all('/[a-z]+/', $this->data, $this->matchesChars);
        preg_match('/[0-9]+/', $this->data, $this->matchesNumbers);

        $this->extractCheckSum();
        $this->sortChars();
    }

    public function extractCheckSum() {
        $this->checkSum = array_pop($this->matchesChars[0]);
    }

    public function isValid() {

        return $this->string === $this->checkSum;
    }

    public function sortChars() {
        $this->allChars = implode($this->matchesChars[0]);
        $this->charInfo = count_chars($this->allChars, 1);

        $keys = array_keys($this->charInfo);
        $values = array_values($this->charInfo);
        array_multisort($values, SORT_DESC, $keys, SORT_ASC);

        $string = [];
        foreach ($keys as $key) {
            $string[] = chr($key);
        }

        $this->string = substr(implode($string), 0, 5);
    }

    public function log() {
        echo '-------------------' . PHP_EOL;
        $this->logValue('data');
        // $this->logValue('matchesNumbers');
        $this->logValue('matchesChars');
        // $this->logValue('checkSum');
        // $this->logValue('charInfo');
        // $this->logValue('allChars');
        // $this->logValue('string');
    }

    public function logValue($key) {
        echo $key . PHP_EOL;
        var_dump($this->$key);
        echo PHP_EOL;
    } 

    public function value() {
        $this->value = intval($this->matchesNumbers[0]);
        return $this->value;
    }

    public function decrypt() {
        foreach ($this->matchesChars[0] as $word) {
            $chars = str_split($word);
            foreach ($chars as $char) {
                $charIndex = ord($char) - 97;
                $newCharIndex = ($charIndex + $this->value()) % 26;
                $newChar = chr($newCharIndex + 97);
                // echo $char, ' ', $charIndex, ' ', $newCharIndex, ' ', $newChar, PHP_EOL;
                echo $newChar;
            }

            echo ' ';
        }

        echo $this->value;

        echo PHP_EOL;
    }
}