<?php

require_once('Line.php');

$input = file_get_contents('data.txt');
$lines = explode("\r\n", $input);

$sum = 0;

foreach ($lines as $line) {

    $lineObj = new Line($line);
    $lineObj->extractData();

    if ($lineObj->isValid()) {
        //$lineObj->log();
        $lineObj->decrypt();
    }
}