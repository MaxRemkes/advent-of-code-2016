<?php

$input = file_get_contents('input.txt');
$lines = explode("\r\n", $input);

$result = '';
$pos = 5;

foreach ($lines as $line) {
    $instructions = str_split($line);

    foreach ($instructions as $instruction) {
        //echo $instruction;
        switch ($pos) {
            case 1:
                if ($instruction == 'D') {
                    $pos = 3;
                }
                break;
            
            case 2:
                if ($instruction == 'R') {
                    $pos = 3;
                }
                else if ($instruction == 'D') {
                    $pos = 6;
                }
                break;
            
            case 3:
                if ($instruction == 'D') {
                    $pos = 7;
                }
                else if ($instruction == 'L') {
                    $pos = 2;
                }
                else if ($instruction == 'R') {
                    $pos = 4;
                }
                else if ($instruction == 'U') {
                    $pos = 1;
                }
                break;
            
            case 4:
                if ($instruction == 'D') {
                    $pos = 8;
                }
                else if ($instruction == 'L') {
                    $pos = 3;
                }
                break;
            
            case 5:
                if ($instruction == 'R') {
                    $pos = 6;
                }
                break;
            
            case 6:
                if ($instruction == 'U') {
                    $pos = 2;
                }
                else if ($instruction == 'R') {
                    $pos = 7;
                }
                else if ($instruction == 'D') {
                    $pos = 'A';
                }
                else if ($instruction == 'L') {
                    $pos = 5;
                }
                break;
            
            case 7:
                if ($instruction == 'U') {
                    $pos = 3;
                }
                else if ($instruction == 'R') {
                    $pos = 8;
                }
                else if ($instruction == 'D') {
                    $pos = 'B';
                }
                else if ($instruction == 'L') {
                    $pos = 6;
                }
                break;
            
            case 8:
                if ($instruction == 'U') {
                    $pos = 4;
                }
                else if ($instruction == 'R') {
                    $pos = 9;
                }
                else if ($instruction == 'D') {
                    $pos = 'C';
                }
                else if ($instruction == 'L') {
                    $pos = 7;
                }
                break;
            
            case 9:
                if ($instruction == 'L') {
                    $pos = 8;
                }
                break;
            
            case 'A':
                if ($instruction == 'U') {
                    $pos = 6;
                }
                else if ($instruction == 'R') {
                    $pos = 'B';
                }
                break;
            
            case 'B':
                if ($instruction == 'U') {
                    $pos = 7;
                }
                else if ($instruction == 'R') {
                    $pos = 'C';
                }
                else if ($instruction == 'D') {
                    $pos = 'D';
                }
                else if ($instruction == 'L') {
                    $pos = 'A';
                }
                break;
            
            case 'C':
                if ($instruction == 'U') {
                    $pos = 8;
                }
                else if ($instruction == 'L') {
                    $pos = 'B';
                }
                break;
            
            case 'D':
                if ($instruction == 'U') {
                    $pos = 'B';
                }
                break;

            //echo $pos;
            

        }

        //echo $instruction;
        //echo $pos . PHP_EOL;
    }
    //echo PHP_EOL . '---' . $pos . '---' . PHP_EOL;
    //echo PHP_EOL;
    echo $pos;
}