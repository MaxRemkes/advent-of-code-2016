<?php

$input = file_get_contents('input.txt');
$lines = explode("\r\n", $input);
$result = 0;
$line_count = 0;

foreach ($lines as $line) {

    // echo PHP_EOL;
    // echo '----------------' . PHP_EOL;
    // echo $line . PHP_EOL;

    $values = [];
    $values[] = getValue($line, 0, 5);
    $values[] = getValue($line, 5, 5);
    $values[] = getValue($line, 10, 5);

    //asort($values);

    if ($values[0] + $values[1] <= $values[2]) {
        // echo $values[0] . ' + ' . $values[1] . ' <= ' . $values[2] . PHP_EOL;
        $result++;
    }
    else if ($values[1] + $values[2] <= $values[0]) {
        // echo $values[1] . ' + ' . $values[2] . ' <= ' . $values[0] . PHP_EOL;
        $result++;
    }
    else if ($values[0] + $values[2] <= $values[1]) {
        // echo $values[0] . ' + ' . $values[2] . ' <= ' . $values[1] . PHP_EOL;
        $result++;
    }

    $line_count++;
}

echo 'result: ' . $result . PHP_EOL;
echo 'line_count: ' . $line_count . PHP_EOL;
echo 'answer: ' . ($line_count - $result);
function getValue($input, $startPos, $length) {
    return intval(trim(substr($input, $startPos, $length)));
}