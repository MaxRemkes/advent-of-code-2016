<?php

$input = file_get_contents('input.txt');
$lines = explode("\r\n", $input);
$correct = 0;
$line_count = 0;
$line_values = [];
for ($i = 0; $i < count($lines); $i++) {
    $line = $lines[$i];

    $values = [];
    $values[] = getValue($line, 0, 5);
    $values[] = getValue($line, 5, 5);
    $values[] = getValue($line, 10, 5);
    $line_values[] = $values;

    if (count($line_values) == 3) {
        for ($j = 0; $j < 3; $j++) {
            $row_values = [$line_values[0][$j], $line_values[1][$j], $line_values[2][$j]];
            

            if (checkIfOk($row_values)) {
                $correct++;
            }
        }

        $line_values = [];
    }

    $line_count++;
}

echo 'correct: ' . $correct . PHP_EOL;

// -------------------------------------------------------
// -------------------------------------------------------
// -------------------------------------------------------

function checkIfOk($values) {
    // var_dump($values);

    $result = false;

    if ($values[0] + $values[1] <= $values[2]) {
        // echo $values[0] . ' + ' . $values[1] . ' <= ' . $values[2] . PHP_EOL;
        $result = true;
    }
    else if ($values[1] + $values[2] <= $values[0]) {
        // echo $values[1] . ' + ' . $values[2] . ' <= ' . $values[0] . PHP_EOL;
        $result = true;
    }
    else if ($values[0] + $values[2] <= $values[1]) {
        // echo $values[0] . ' + ' . $values[2] . ' <= ' . $values[1] . PHP_EOL;
        $result = true;
    }

    return !$result;
}

function getValue($input, $startPos, $length) {
    return intval(trim(substr($input, $startPos, $length)));
}