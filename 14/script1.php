<?php

$salt = 'abc';

$santaKeys = [];
$possibleSantaKeys = [];

for ($i = 0; $i < 30000; $i++) {

    $possibleKey = md5($salt . $i);

    $isQuint = false;
    if (count($possibleSantaKeys) > 0 && preg_match('/(.)\1{4}/', $possibleKey, $matches)) {

        $isQuint = true;

//        logToScreen($possibleKey);
//        logToScreen('Has more than zero triples and has quint ' . $matches[1] . ' at index ' . $i);

        $keysToBeDeleted = [];
        foreach ($possibleSantaKeys as $santaKeyIndex => $possibleSantaKey) {
            if ($possibleSantaKey->stillValidIndex($i)) {
                if ($possibleSantaKey->isMatchingQuint($i, $matches[1], $possibleKey)) {
                    logToScreen('Adding quint ' . $matches[1] . ' at ' . $i . ' triple at ' . $possibleSantaKey->tripleIndex
                        . ' - ' . $possibleKey);
                    $santaKeys[] = $possibleSantaKey;
                    $keysToBeDeleted[] = $santaKeyIndex;
                    break;
                }
            }
            else {
//                logToScreen('Not valid triple currentIndex ' . $i . ', tripleIndex: ' . $possibleSantaKey->tripleIndex);
                $keysToBeDeleted[] = $santaKeyIndex;
            }
        }

        var_dump($keysToBeDeleted);
        var_dump($possibleSantaKeys);
        foreach ($keysToBeDeleted as $keyToDelete) {
            unset($possibleSantaKeys[$keyToDelete]);
        }
        var_dump($possibleSantaKeys);
        echo PHP_EOL, PHP_EOL;
    }

    if (!$isQuint && preg_match('/(.)\1{2}/', $possibleKey, $matches)){

        $possibleSantaKey = new SantaKey($i, $matches[1], $possibleKey);
        $possibleSantaKeys[$i] = $possibleSantaKey;

//        logToScreen('Adding ' . $matches[1] . ' at ' . $i . ' ' . $possibleKey);
    }
}

function logToScreen($msg) {
//    echo $msg, PHP_EOL;
}

echo PHP_EOL;
//var_dump($santaKeys);
echo count($santaKeys);

class SantaKey {
    public $triple;
    public $tripleIndex;
    public $tripleHash;
    public $quintIndex;
    public $quintHash;

    function __construct($tripleIndex, $triple, $tripleHash) {
        $this->tripleIndex = $tripleIndex;
        $this->triple = $triple;
        $this->tripleHash = $tripleHash;
    }

    public function stillValidIndex($currentIndex) {
        $diff = $currentIndex - $this->tripleIndex;
        $result = $diff <= 1000;
//        echo $currentIndex, ' - ', $this->tripleIndex, ' = ', $diff, ' result: ', var_export($result, true), PHP_EOL;

        return $result;
    }

    public function isMatchingQuint($quintIndex, $quint, $quintHash) {

        $result = false;

        if ($quint == $this->triple) {
            $this->quintIndex = $quintIndex;
            $this->quintHash = $quintHash;
            $result = true;
        }

        return $result;
    }
}