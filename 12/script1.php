<?php

$input = file_get_contents('input.txt');
$lines = explode("\n", $input);

//var_dump($lines);
$steps = 0;

$data = ['a' => 0, 'b' => 0, 'c' => 1, 'd' => 0];

for ($i = 0; $i < count($lines); $i++) {
    $line = $lines[$i];
//    echo $i, PHP_EOL, $line, PHP_EOL;
    if (preg_match('/([a-z]+) ([a-z0-9]+) ?([-a-z0-9]+)?/', $line, $matches)) {

        if ($matches[1] == 'cpy') {
            if (is_numeric($matches[2])) {
                $data[$matches[3]] = intval($matches[2]);
            }
            else {
                $data[$matches[3]] = $data[$matches[2]];
            }
        }
        else if ($matches[1] == 'inc') {
            $data[$matches[2]]++;
        }
        else if ($matches[1] == 'dec') {
            $data[$matches[2]]--;
        }
        else if ($matches[1] == 'jnz') {
            if (is_numeric($matches[2])) {
                if (intval($matches[2]) === 0) {
//                    echo 'jnz x = 0, skipping', PHP_EOL, PHP_EOL;
//                    sleep(1);
                    continue;
                }
            }
            else {

                if ($data[$matches[2]] === 0) {
//                    echo 'jnz ', $matches[2], ' = 0, skipping', PHP_EOL, PHP_EOL;
//                    sleep(1);
                    continue;
                }
            }

//            echo 'new i: ', $i;

            $skip = false;
            if (is_numeric($matches[3])) {
                $skip = intval($matches[3]);
            }
            else {
                $skip = $data[$matches[3]];
            }

//            echo 'skip: ', $skip, PHP_EOL;

            if ($skip > 0) {
                $i = $i + $skip - 1;
            }
            else {
                $i = $i + $skip - 1;
            }

//            echo ' + ', $skip, ' = ', $i, PHP_EOL;
        }
    }

//    if ($steps % 10000 === 0) {

//        $log = [];
//        foreach ($data as $key => $value) {
//            $log[] = $key . ': ' . $value;
//        }
//        echo implode($log, ' | '), PHP_EOL, PHP_EOL;
//        sleep(1);
//    }

    $steps++;
}



var_dump($data);