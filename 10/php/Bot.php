<?php

class Bot {
    public $id;
    public $lowId;
    public $lowOutput;
    public $highId;
    public $highOutput;
    public $data;

    function __construct($id, $lowId, $lowOutput, $highId, $highOutput) {
        $this->id = $id;
        $this->lowId = $lowId;
        $this->lowOutput = $lowOutput;
        $this->highId = $highId;
        $this->highOutput = $highOutput;
        $this->data = [];
    }

    public function addValue($value) {
        $this->data[] = $value;
    }

    public function shiftHigh() {
        asort($this->data);
        return array_pop($this->data);
    }

    public function shiftLow() {
        asort($this->data);
        return array_shift($this->data);
    }

    public function shift() {
        return array_shift($this->data);
    }

    public function isFull() {
        return count($this->data) == 2;
    }
}