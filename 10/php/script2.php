<?php

require 'Bot.php';

$input = file_get_contents('input.txt');
$lines = explode("\n", $input);
$bots = [];
$instructions = [];
$outputs = [];
$logs = [];

//var_dump($lines);

foreach ($lines as $line) {
    $matches;
    if (preg_match('/^bot (\d+) gives low to (bot|output) (\d+) and high to (bot|output) (\d+)$/', $line, $matches)) {
        $botId = intval($matches[1]);
        $lowOutput = $matches[2] == 'bot' ? 0 : 1;
        $lowId = intval($matches[3]);
        $highOutput = $matches[4] == 'bot' ? 0 : 1;
        $highId = intval($matches[5]);

        $bots[$botId] = new Bot($botId, $lowId, $lowOutput, $highId, $highOutput);
    }
    else if (preg_match('/^value (\d+) goes to bot (\d+)$/', $line, $matches)) {
        $value = intval($matches[1]);
        $botId = intval($matches[2]);
        $instructions[] = [$value, $botId];
    }
}

// var_dump($bots);

foreach ($instructions as $instruction) {
    
    $bot = $bots[$instruction[1]];

    $bot->addValue($instruction[0]);

    if ($bot->isFull()) {
        handleFullBot($bot);
    }
    else {
        $log = sprintf('value %d goes to bot %d', $instruction[0], $instruction[1]);
        echo $log, PHP_EOL;
        $logs[] = $log;
    }

    $bots[$instruction[1]] = $bot;
}

function handleFullBot($bot) {

    global $bots;
    global $outputs;
    global $logs;

    $oldData = $bot->data;

    $highValue = $bot->shiftHigh();
    $lowValue = $bot->shift();

    $log = sprintf('bot %d gives low (%d) to %s %d and high (%d) to %s %d', $bot->id,
        $lowValue, $bot->lowOutput == 0 ? 'bot' : 'output', $bot->lowId,
        $highValue, $bot->highOutput == 0 ? 'bot' : 'output', $bot->highId
    );

    echo $log, PHP_EOL;

    if ($bot->lowOutput == 0) {
        $botLow = $bots[$bot->lowId];
        $botLow->addValue($lowValue);

        if ($botLow->isFull()) {
            $log = sprintf('bot %d is full', $botLow->id);
            echo $log, PHP_EOL;
            handleFullBot($botLow);
        }
    }
    else {
        $outputs[$bot->lowId] = $lowValue;
    }
    if ($bot->highOutput == 0) {
        $botHigh = $bots[$bot->highId];
        $botHigh->addValue($highValue);

        if ($botHigh->isFull()) {
            $log = sprintf('bot %d is full', $botHigh->id);
            echo $log, PHP_EOL;
            handleFullBot($botHigh);
        }
    }
    else {
        $outputs[$bot->highId] = $highValue;
    }

    // $log = sprintf('full bot %d high %d to %d (%d), low %d to %d (%d)', $bot->id, $highValue, $bot->highId, $bot->highOutput, $lowValue, $bot->lowId, $bot->lowOutput);
    
    $logs[] = $log;
}

// echo implode(PHP_EOL, $logs);
// echo PHP_EOL;
// var_dump($outputs[0]);
// var_dump($outputs[1]);
// var_dump($outputs[2]);

echo 'The answer is ', $outputs[0] * $outputs[1] * $outputs[2];