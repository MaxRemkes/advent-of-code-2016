class Turtle:
    x = 0
    y = 0
    direction = 0

    def move(self, instruction):
        if instruction[:1] == 'R':
            self.direction+= 1
        else:
            self.direction-= 1

        steps = int(instruction[1:])
        moveDirection = self.direction % 4

        if moveDirection == 0:
            self.y += steps
        elif moveDirection == 1:
            self.x += steps
        elif moveDirection == 2:
            self.y -= steps
        else:
            self.x -= steps

    def distance(self):
        return abs(self.x) + abs(self.y)