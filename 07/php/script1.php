<?php

require_once('TlsChecker.php');

$input = file_get_contents('input.txt');
$lines = explode("\n", $input);

$count = 0;

foreach ($lines as $line) {
    $checker = new TlsChecker;
    $count += $checker->check($line) ? 1 : 0;
}
echo PHP_EOL;
echo $count, ' IP(s) support TLS', PHP_EOL;