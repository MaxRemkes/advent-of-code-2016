<?php

class SslChecker {

    protected $input;
    protected $parts;
    protected $bracketParts;
    protected $ok;
    protected $abas;
    protected $babs;

    function __construct() {
        $this->parts = [];
        $this->bracketParts = [];
        $this->abas = [];
        $this->babs = [];
        $this->babsReversed = [];
        $this->ok = true;
    }

    public function check($input) {
        $this->input = trim($input);

        $this->extractParts();

        $this->doCheck();

        //echo 'ok: ', $this->ok ? 'ok' : 'not ok', '-------------', PHP_EOL;
        //echo '-------------', PHP_EOL;

        if ($this->ok) {
            echo 'input: ', $this->input, PHP_EOL;

            echo 'abas', PHP_EOL;
            var_dump($this->abas);
            echo 'babsReversed', PHP_EOL;
            var_dump($this->babsReversed);

        }

        return $this->ok;
    }

    protected function extractParts() {
        $matches;
        preg_match_all('/[a-z]+/', $this->input, $matches);

        $this->parts = $matches[0];
        
        $bracketMatches;
        preg_match_all('/\[[a-z]+\]/', $this->input, $bracketMatches);

        foreach ($bracketMatches[0] as $part) {
            $this->bracketParts[] = $this->removeBrackets($part);
        }
    }

    protected function doCheck() {

        foreach ($this->parts as $part) {

            if (in_array($part, $this->bracketParts)) {
                continue;
            }

            $this->abas = array_merge($this->findAbasOrBabs($part), $this->abas);
        }

        foreach ($this->bracketParts as $part) {
            $babs = $this->findAbasOrBabs($part);
            $this->babs = array_merge($babs, $this->babs);
            $this->babsReversed = array_merge($this->reverseAbasOrBabs($babs), $this->babsReversed);
        }

        $this->ok = count($this->abas) > 0 && count($this->babs) && $this->anAbaHasValidBab();

        return $this->ok;
    }

    protected function anAbaHasValidBab() {

        foreach ($this->abas as $aba) {
            if (in_array($aba, $this->babsReversed)) {
                return true;
            }
        }

        return false;
    }

    protected function findAbasOrBabs($part) {
        
        $checkCount = strlen($part) - 2;
        $result = [];
        
        for ($i = 0; $i < $checkCount; $i++) {
            $possible = substr($part, $i, 3);
            // echo 'possible: ', $possible, ' isAbaOrBab: ', $this->isAbaOrBab($possible) ? 'yes' : 'no', PHP_EOL;
            if ($this->isAbaOrBab($possible)) {
                $result[] = $possible;
            }
        }

        return $result;
    }

    protected function removeBrackets($input) {
        return substr($input, 1, strlen($input) - 2);
    }

    protected function isAbaOrBab($input) {

        $chars = str_split($input);
        return $chars[0] == $chars[2] && $chars[0] != $chars[1];
    }

    protected function reverseAbasOrBabs($input) {
        $result = [];
        foreach ($input as $element) {
            $element = str_split($element);
            $result[] = $element[1] . $element[0] . $element[1];
        }

        return $result;
    }
}