<?php

class TlsChecker {

    protected $input;
    protected $parts;
    protected $bracketParts;
    protected $ok;

    function __construct() {
        $this->parts = [];
        $this->bracketParts = [];
        $this->ok = true;
    }

    public function check($input) {
        $this->input = trim($input);
        echo 'input: ', $this->input, PHP_EOL;

        $this->extractParts();

        $this->doCheck();

        echo 'ok: ', $this->ok ? 'ok' : 'not ok', PHP_EOL;
        echo '-------------', PHP_EOL;

        return $this->ok;
    }

    protected function extractParts() {
        $matches;
        preg_match_all('/[a-z]+/', $this->input, $matches);

        $this->parts = $matches[0];
        
        $bracketMatches;
        preg_match_all('/\[[a-z]+\]/', $this->input, $bracketMatches);

        foreach ($bracketMatches[0] as $part) {
            $this->bracketParts[] = $this->removeBrackets($part);
        }
    }

    protected function doCheck() {

        $partsHasAbba = false;
        foreach ($this->parts as $part) {

            if (in_array($part, $this->bracketParts)) {
                continue;
            }

            if ($this->hasAbba($part)) {
                $partsHasAbba = true;
                break;
            }
        }

        $bracketPartsHasAbba = false;
        foreach ($this->bracketParts as $part) {
            if ($this->hasAbba($part)) {
                $bracketPartsHasAbba = true;
                break;
            }
        }

        echo 'partsHasAbba: ', $partsHasAbba ? 'yes' : 'no', ', bracketPartsHasAbba: ', $bracketPartsHasAbba ? 'yes' : 'no', PHP_EOL;

        $this->ok = $partsHasAbba && !$bracketPartsHasAbba;
    }

    protected function hasAbba($part) {
        
        $checkCount = strlen($part) - 3;
        for ($i = 0; $i < $checkCount; $i++) {
            $possibleAbba = substr($part, $i, 4);
            echo 'possibleAbba: ', $possibleAbba, ' isAbba: ', $this->isAbba($possibleAbba) ? 'yes' : 'no', PHP_EOL;
            if ($this->isAbba($possibleAbba)) {
                return true;
            }
        }

        return false;
    }

    protected function removeBrackets($input) {
        return substr($input, 1, strlen($input) - 2);
    }

    protected function isAbba($input) {

        $partA = substr($input, 0, 2);
        $partB = strrev(substr($input, 2));
        
        return $partA[0] != $partA[1] && $partA === $partB;
    }
}