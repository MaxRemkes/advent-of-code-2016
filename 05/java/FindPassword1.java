import java.security.*;
import java.io.*;
import java.math.*;

public class FindPassword1 {

    public static String toHex(String arg) {
        String result = "";

        try {
            result = String.format("%040x", new BigInteger(1, arg.getBytes("UTF-8")));
        }
        catch (UnsupportedEncodingException e) {
            System.out.println(e.getMessage());
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return result;
    }

    public static String md5(String input) throws NoSuchAlgorithmException {
        String result = input;
        if(input != null) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            result = hash.toString(16);
            while(result.length() < 32) {
                result = "0" + result;
            }
        }
        return result;
    }

    public static void main(String[] args) {

        String input = "reyedfim";
        StringBuilder password = new StringBuilder();

        System.out.println("input is: " + input);

        int ix = 0;

        while(password.toString().length() < 8) {
            try {
                String attempt = input + String.valueOf(ix);
                String hash = md5(attempt);
                //System.out.println("attempt: " + attempt + ", hash: " + hash + ", ix: " + ix + ", substr: " + hash.substring(0, 5) + ", password: " + password);
                if (hash.substring(0, 5).equals("00000")) {
                    password.append(hash.substring(5, 6));
                    System.out.println(ix + ": " + password);
                }
            }
            catch (NoSuchAlgorithmException e) {
                System.out.println(e.getMessage());
            }

            ix++;
        }

        System.out.println("password: " + password);
    }
} 