<?php

define('PASSWORD_LENGTH', 8);

$input = 'reyedfim';

$ix = 0;
$password = [];

while (count($password) < PASSWORD_LENGTH) {

    $hash = md5($input . $ix);

    if (preg_match('/^[0]{5}/', $hash)) {

        $position = substr($hash, 5, 1);

        if (is_numeric($position)) {

            $position = intval($position);

            if ($position < PASSWORD_LENGTH && !isset($password[$position])) {
                $password[$position] = substr($hash, 6, 1);
            
                echo count($password), ': ', str_pad($ix, 10, ' ', STR_PAD_LEFT), ' ', $hash, ' ', $position, ' ', $password[$position], PHP_EOL;
            }
        }
    }

    $ix++;
}

ksort($password);
echo 'password is: ', implode($password), PHP_EOL;
