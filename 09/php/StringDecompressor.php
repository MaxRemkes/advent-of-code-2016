<?php

class StringDecompressor {
    public $output;
    protected $input;
    protected $time;

    function __construct($input) {
        $this->time = time();
        $this->input = $input;
    }

    function doDecompressionVersion1() {
        $this->decompress($this->input);
    }

    function doDecompressionVersion2() {

        // uses too much memory!!
        
        $input = $this->decompress($this->input);

        $ix = 0;
        while (preg_match('/\(\d+x\d+\)+/', $input)) {
            // echo 'input: ', $input, PHP_EOL;
            $input = $this->decompress($input);
            file_put_contents('output/' . $this->time . '_' . str_pad($ix, 2, '0', STR_PAD_LEFT) . '.txt', $input);
            echo '+';

            if ($ix == 3) {
                exit;
            }

            $ix++;
        }

        echo PHP_EOL;
    }

    public function decompress($input) {

        $result = [];

        $inputIt = new StringIterator($input);
        $compressInstruction = [];

        foreach ($inputIt as $char) {
            if (count($compressInstruction) > 0) {
                $compressInstruction[] = $char;
                if ($char === ')') {
                    $matches;
                    if (preg_match('/^\((\d+)x(\d+)\)$/', implode($compressInstruction), $matches)) {
                        $result[] = $inputIt->getSegment(intval($matches[1]), intval($matches[2]));
                    }

                    $compressInstruction = [];
                }
            }
            else if ($char === '(') {
                $compressInstruction[] = $char;
            }
            else {
                $result[] = $char;
            }
        }

        $this->output = implode($result);

        return $this->output;
    }

    public function answer() {
        return strlen($this->output);
    }
}