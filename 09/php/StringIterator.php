<?php

class StringIterator implements Iterator {
    private $position = 0;
    private $array;

    public function __construct($value) {
        $this->position = 0;
        $this->array = str_split($value);
    }

    function rewind() {
        $this->position = 0;
    }

    function current() {
        return $this->array[$this->position];
    }

    function key() {
        return $this->position;
    }

    function next() {
        ++$this->position;
    }

    function valid() {
        return isset($this->array[$this->position]);
    }

    function getSegment($length, $count) {
        $string = [];
        for ($i = 0; $i < $length; $i++) {
            $string[] = $this->array[$this->position + 1];
            $this->position++;
        }

        $segment = [];
        for ($i = 0; $i < $count; $i++) {
            $segment[] = implode($string);
        }

        return implode($segment);
    }

}