<?php

ini_set('memory_limit', '6G');

require_once('StringIterator.php');
require_once('StringDecompressor.php');

// $input = '(3x3)XYZ';
// $input = 'X(8x2)(3x3)ABCY';
// $input = '(27x12)(20x12)(13x14)(7x10)(1x12)A';
// $input = '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN';

$input = file_get_contents('input.txt');

$decompressor = new StringDecompressor($input);
$decompressor->doDecompressionVersion2();

echo 'Input: ', $input, PHP_EOL;
echo 'The decompressed string: ', $decompressor->output, PHP_EOL;
echo 'The decompressed string length is: ', $decompressor->answer(), PHP_EOL;