<?php

// !!! Doesn't work for puzzle input !!!

// $input = '(3x3)XYZ';
// $input = 'X(8x2)(3x3)ABCY';
// $input = '(27x12)(20x12)(13x14)(7x10)(1x12)A';
$input = '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN';

// $input = file_get_contents('input.txt');

echo 'input: ', $input, PHP_EOL;

$total = 0;

calculateSectionLength($input);

function calculateSectionLength($input) {

    global $total;

    $search = $input;

    while (hasMarker($search, $marker, $length, $count, $leftover)) {
        $total += $leftover;
        echo 'adding: ', $leftover, ' leftover, sum: ', $total, PHP_EOL;
        $part = substr($search, $leftover, $length + strlen($marker));
        echo 'part: ', $part, PHP_EOL;

        $total += calculateLength($part, 0);
        $search = substr($search, strpos($search, $marker) + strlen($marker) + $length);
    }

    echo 'total: ', $total + strlen($search), PHP_EOL;

}

function hasMarker($input, &$marker, &$length, &$count, &$leftover) {
    
    $regexResult = preg_match('/([a-z]?)(\((\d+)x(\d+)\))/i', $input, $matches);

    if ($regexResult) {
        $leftover = strlen($matches[1]);
        $marker = $matches[2];
        $length = intval($matches[3]);
        $count = intval($matches[4]);
    }

    return $regexResult;
}

function calculateLength($input) {

    echo 'calculateLength: ', $input, PHP_EOL;
    $sum = 0;

    $length = 0;
    $count = 0;
    $marker = '';
    $leftover = '';

    if (hasMarker($input, $marker, $length, $count, $leftover)) {
        
        $substringed = substr($input, strpos($input, $marker) + strlen($marker));
        $sum = calculateLength($substringed);
    }
    else {
        return 0;
    }

    $markerPosLeft = strpos($input, '(', strpos($input, '(') + 1);
    $markerPosRight = strpos($input, ')') + 1;

    

    //echo 'input: ', $input, ', markerPosLeft: ', var_export($markerPosLeft, true), ' length: ', $length, PHP_EOL;

    if ($sum > 0) {
        

        if ($markerPosRight + $length <= $markerPosLeft) {
            echo 'calculateLength count * (length) + sum: ', $count, ' * ', ($length), ' + ', $sum, ' = ', ($count * ($length - 1)) + $sum, PHP_EOL;
            return ($count * ($length)) + $sum;
        }

        echo 'calculateLength count * sum: ', $count, ' * ', $sum, ' = ', $count * $sum, PHP_EOL;

        return $count * $sum;
    }

    echo 'calculateLength count * length: ', $count, ' * ', $length, ' = ', $count * $length, PHP_EOL;

    return $count * $length;
}