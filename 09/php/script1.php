<?php

require_once('StringIterator.php');
require_once('StringDecompressor.php');

// $input = 'A(1x5)BC';
// $input = '(3x3)XYZ';
// $input = 'A(2x2)BCD(2x2)EFG';
// $input = '(6x1)(1x3)A';
// $input = 'X(8x2)(3x3)ABCY';

$input = file_get_contents('input.txt');

$decompressor = new StringDecompressor($input);
$decompressor->doDecompressionVersion1();

echo 'Input: ', $input, PHP_EOL;
echo 'The decompressed string: ', $decompressor->output, PHP_EOL;
echo 'The decompressed string length is: ', $decompressor->answer(), PHP_EOL;