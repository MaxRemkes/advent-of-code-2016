<?php

// https://github.com/rhardih/aoc/blob/master/2016/9p2.c

// $input = '(3x3)XYZ';
// $input = 'X(8x2)(3x3)ABCY';
// $input = '(27x12)(20x12)(13x14)(7x10)(1x12)A';
// $input = '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN';

$input = file_get_contents('input.txt');

// echo 'input: ', $input, PHP_EOL;

$values = [];
for ($i = 0; $i < strlen($input); $i++) {
    $values[$i] = 1;
}

$total = 0;
$inputArr = str_split($input);

for ($i = 0; $i < strlen($input); $i++) {
    
    $char = $inputArr[$i];

    if ($char == '(') {
        $matches;
        preg_match('/^\((\d+)x(\d+)\)/', substr($input, $i), $matches);
        $length = intval($matches[1]);
        $multiplier = intval($matches[2]);
        // var_dump($matches);
        $closingBracketPos = $i + strlen($matches[0]) - 1;
        // echo 'length: ', $length, ' multiplier: ', $multiplier,
            // ' closingBracketPos: ', $closingBracketPos, PHP_EOL;
        for ($j = $closingBracketPos + 1; $j < $closingBracketPos + 1 + $length; $j++) {
            $values[$j] *= $multiplier;
        }

        $i = $closingBracketPos;
    }
    else {
        $total += $values[$i];
    }
}

// var_dump($values);

echo 'total: ', $total, PHP_EOL;