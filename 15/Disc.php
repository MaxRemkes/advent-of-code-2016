<?php

class Disc {
    public $positions;
    public $id;
    public $startPosition;
    public $currentPosition;

    function __construct($id, $positions, $startPosition) {
        $this->id = $id;
        $this->positions = $positions;
        $this->startPosition = $startPosition;
        $this->currentPosition = $startPosition;
    }

    public function step() {
//        echo 'Stepping ', $this->id, ' currentPosition ', $this->currentPosition, PHP_EOL;
        $this->currentPosition++;
        return $this->positions % $this->currentPosition;
    }

    public function peekStep($peekLength) {
//        echo 'Peeking ', $this->id, ' currentPosition: ', $this->currentPosition % $this->positions,
//            ' peekLength ', $peekLength, ' peekResult ', (($this->currentPosition + $peekLength) % $this->positions),
//            PHP_EOL;
        return (($this->currentPosition + $peekLength) % $this->positions) === 0;
    }
}