<?php

require_once 'Disc.php';

$discs = [];

//$disc = new Disc(0, 5, 4);
//$discs[] = $disc;
//$disc = new Disc(1, 2, 1);
//$discs[] = $disc;

$data = [
    [13, 11],
    [5,0],
    [17,11],
    [3,0],
    [7,2],
    [19,17],
    [11,0]
];

for ($i = 0; $i < count($data); $i++) {
    $disc = new Disc($i, $data[$i][0], $data[$i][1]);
    $discs[] = $disc;
}

$puzzleSolved = false;
for ($i = 0; !$puzzleSolved; $i++) {
//    echo 'Time = ', $i, PHP_EOL;
    if (checkSolved($discs)) {
        echo 'answer: ', $i, PHP_EOL;
        $puzzleSolved = true;
    }

    for ($j = 0; $j < count($discs); $j++) {
//        echo $discs[$j]->currentPosition % $discs[$j]->positions, '(', $discs[$j]->positions, ') ';
        $discs[$j]->step();
    }

//    echo PHP_EOL, PHP_EOL;
}

function checkSolved($discs) {
    $solved = true;
    for ($i = 0; $i < count($discs); $i++) {
        if (!$discs[$i]->peekStep($i + 1)) {
            $solved = false;
            break;
        }
//        echo '---';
    }

//    echo PHP_EOL;

    return $solved;
}