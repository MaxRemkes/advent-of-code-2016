<?php

require_once('Display.php');

$input = file_get_contents('input.txt');
$lines = explode("\n", $input);

$count = 0;

// $display = new Display(7, 3);
$display = new Display(50, 6);

foreach ($lines as $line) {
    $display->parseInstruction($line);
    $display->outputScreen();
}

echo PHP_EOL, $display->countPixelsLit(), ' pixels are lit' . PHP_EOL;