<?php

class Display {

    protected $width;
    protected $height;
    protected $screen;

    function __construct($width, $height) {
        $this->width = $width;
        $this->height = $height;

        $this->initScreen();
    }

    function initScreen() {
        $this->screen = [];
        for ($y = 0; $y < $this->height; $y++) {
            $this->screen[$y] = [];
            for ($x = 0; $x < $this->width; $x++) {
                $this->screen[$y][$x] = 0;
            }
        }
    }

    public function parseInstruction($instruction) {

        //echo 'instruction: ', $instruction, PHP_EOL;

        $matches;
        if (preg_match('/^rect\s(\d+)x(\d+)$/', $instruction, $matches)) {
            $this->drawRect(intval($matches[1]), intval($matches[2]));
        }
        else if (preg_match('/^rotate\s(row|column)\s[xy]=(\d+)\sby\s(\d+)$/', $instruction, $matches)) {
            if ($matches[1] == 'row') {
                $this->rotateRow(intval($matches[2]), intval($matches[3]));
            }
            else {
                $this->rotateColumn(intval($matches[2]), intval($matches[3]));
            }
        }
    }

    protected function drawRect($width, $height) {
        // rect AxB turns on all of the pixels in a rectangle at the top-left of the screen which is A wide and B tall.
        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $this->screen[$y][$x] = 1;
            }
        }
    }

    protected function rotateRow($row, $count) {
        // shifts all of the pixels in row (0 is the top row) right by x pixels
        $oldRow = $this->screen[$row];
        $newRow = [];
        for ($x = 0; $x < $this->width; $x++) {
            $newRow[$x] = 0;
        }

        for ($x = 0; $x < $this->width; $x++) {
            if ($oldRow[$x] === 1) {
                $newValue = ($x + $count) % $this->width;
                $newRow[$newValue] = 1;
                //echo 'x: ', $x, ' newValue: ', $newValue, ' count: ', $count, ' width: ', $this->width, PHP_EOL;
            }
        }

        $this->screen[$row] = $newRow;
    }

    protected function rotateColumn($column, $count) {

        $oldColumn = [];
        $newColumn = [];
        for ($y = 0; $y < $this->height; $y++) {
            $oldColumn[$y] = $this->screen[$y][$column];
        }

        for ($y = 0; $y < $this->height; $y++) {
            if ($oldColumn[$y] === 1) {
                $newValue = ($y + $count) % $this->height;
                $newColumn[$newValue] = 1;
            }
        }

        for ($y = 0; $y < $this->height; $y++) {
            if (isset($newColumn[$y])) {
                $this->screen[$y][$column] = 1;
            }
            else {
                $this->screen[$y][$column] = 0;
            }
        }
    }

    public function outputScreen() {

        //echo chr(27) . '[2J';

        for ($y = 0; $y < $this->height; $y++) {
            for ($x = 0; $x < $this->width; $x++) {
                echo $this->screen[$y][$x] === 1 ? '#' : '.';
            }

            echo PHP_EOL;
        }

        echo PHP_EOL;

        //usleep(100000);
    }

    public function countPixelsLit() {
        $sum = 0;
        for ($y = 0; $y < $this->height; $y++) {
            $sum += array_sum($this->screen[$y]);
        }

        return $sum;
    }
}