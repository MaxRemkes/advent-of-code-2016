<?php

ini_set('memory_limit', '7000M');

//$count = 3014387;
$startCount = 5;

$elves = [];
for ($i = 0; $i < $startCount; $i++) {
    $elves[$i] = $i;
}
$elfIndex = 0;
while (count($elves) > 1) {
    usleep(100000);
    echo '----------', PHP_EOL;
    printElves($elves);

    $half = floor(count($elves) / 2);
    echo 'half: ', $half, PHP_EOL;
    //echo 'pos: ', $elfIndex % count($elves), PHP_EOL;
    if ($elfIndex > count($elves) - 1) {
        $elfIndex = 0;
    }
    echo 'elfIndex: ', $elfIndex, PHP_EOL;

    $deleteIndex = $elfIndex + $half;
    echo 'deleteIndex: ', $deleteIndex, PHP_EOL;
    unset($elves[$deleteIndex]);

    $elves = array_values($elves);

    $elfIndex++;

}

var_dump($elves);

function printElves(&$elves) {
    echo implode(' ', $elves), PHP_EOL;
}
