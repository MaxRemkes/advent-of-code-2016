<?php

// https://en.wikipedia.org/wiki/Josephus_problem

$winner = winner(3014387);
echo 'winner: ', $winner, PHP_EOL;

function winner($n) {
    $w = 1;
//    $i;

    for ($i=1; $i<$n; $i++) {
        $w = $w % $i + 1;
        if ($w > ($i + 1)/2) {
            $w++;
        }
    }
    return ($w);
}