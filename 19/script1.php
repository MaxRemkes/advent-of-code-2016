<?php

ini_set('memory_limit', '7000M');

//$count = 3014387;
$count = 10;
$elves = [];

for ($i = 0; $i < $count; $i++) {
    $elves[$i] = 1;
}

$solved = false;
$elfIndex = 0;

while ($solved == false) {

    $curIndex = $elfIndex % $count;
    $elfIndex++;

//    echo 'nextIndex: ', $nextIndex, PHP_EOL;
//    echo 'curIndex: ', $curIndex, PHP_EOL;


    if ($elves[$curIndex] !== 0) {
        for ($i = $elfIndex; $i < $elfIndex + $count; $i++) {
            $nextIndex = $i % $count;

            if ($elves[$nextIndex] === 0) {
                continue;
            }
            else {
                $elves[$curIndex] += $elves[$nextIndex];
                $elves[$nextIndex] = 0;
                break;
            }
        }
    }
    else {
        continue;
    }

    echo 'elf ', ($curIndex + 1), ' has ', $elves[$curIndex], ' presents', PHP_EOL;

    if ($elves[$curIndex] === $count) {
        echo 'the answer is ', ($curIndex + 1), ' with elfIndex ', $elfIndex, PHP_EOL;
        $solved = true;
    }
}