<?php

class ElfCircle
{
    protected $position;
    protected $count;
    protected $elves;

    function __construct($count)
    {
        $this->count = $count;
        $this->elves = [];
        $this->position = 0;

        for ($i = 0; $i < $this->count; $i++) {
            $this->elves[$i] = true;
        }
    }

    public function log()
    {
        $log = array();
        for ($i = 0; $i < count($this->elves); $i++) {
//            $log[] = str_pad($i, 3, '0', STR_PAD_LEFT) . ' ' . var_export($this->elves[$i], true);
            $log[] = $i . ': ' . var_export($this->elves[$i], true);
        }

        echo implode($log, ', '), PHP_EOL;
    }

    public function removeNextHalfElf()
    {
        while ($this->elves[$this->position] === false) {
            $this->position++;

            if (!isset($this->elves[$this->position])) {
                $this->position = 0;
            }
        }

        $position = $this->position + 1;
        $half = $this->half();
//        echo 'current position: ', $this->position, ', half: ', $half, PHP_EOL;
        $positionToDelete = -1;

//        $positions = [];
        while ($half > 0) {
//            $positions[] = $position;
            if (!isset($this->elves[$position])) {
                $position = 0;
                $positionToDelete = 0;
                continue;
            }
            else {
                if ($this->elves[$position] === true) {
                    $half--;
                    $positionToDelete = $position;
                }
            }

            $position++;
        }

//        echo 'positions: ', implode(', ', $positions), PHP_EOL;
//        echo 'positionToDelete: ', $positionToDelete, PHP_EOL, PHP_EOL;

        $this->remove($positionToDelete);
        $this->position++;
    }

    protected function half() {
        return floor($this->count / 2);
    }

    protected function remove($index) {

        $this->elves[$index] = false;
        $this->count--;

//        if ($this->count % 1000 === 0) {
//            echo '+';
//        }
        echo $this->count, PHP_EOL;
    }

    public function oneLeft() {
        return $this->count === 1;
    }

    public function answer()
    {
        $answer = -1;
        for ($i = 0; $i < count($this->elves); $i++) {
            if ($this->elves[$i] === true) {
                $answer = $i;
                break;
            }
        }

        echo PHP_EOL, PHP_EOL, 'answer is: ', $answer + 1, PHP_EOL;
    }
}