<?php

define('FAV', 1358);

require 'Point.php';

$width = 50;
$height = 50;

$favNum = 1358;

//echo '   ';
//for ($x = 0; $x < $width; $x++) {
//    echo $x % 10;
//}
//echo PHP_EOL;
//
//echo '   ';
//for ($x = 0; $x < $width; $x++) {
//    echo floor($x / 10);
//}
//echo PHP_EOL;

$queue = [];

findPoint(1, 1, 31, 39);
//findPoint(1, 1, 7, 4);

function findPoint($startX, $startY, $endX, $endY) {
    /*
    Voeg de wortel van de graaf toe aan de queue
    Als er een knoop in de queue zit, neem deze uit de queue en bekijk de knoop:
    Als dit een oplossing is: stop het zoeken en geef de oplossing
    Als dit geen oplossing is: voeg alle kinderen van deze knoop toe aan het einde van de FIFO queue
    Als de queue leeg is: alle knopen zijn bekeken dus stop het zoeken en geef aan dat er geen oplossing is
    Ga door naar stap 2
    */

    $queue[] = new Point($startX, $startY, 0);
    $visited = [];
    handleQueue($queue, $visited, $endX, $endY);
}

function handleQueue($queue, $visited, $endX, $endY) {

    if (count($queue) === 0) {
        return;
    }
    $node = array_shift($queue);

    $visited[] = $node->toString();

//    echo $node->toString(), PHP_EOL;

    if ($node->x == $endX && $node->y == $endY) {
        echo '--- ', $node->depth, PHP_EOL;
    }

    $top = new Point($node->x, $node->y - 1, $node->depth + 1);
    if ($top->isValid() && $top->isEven() && !in_array($top->toString(), $visited)) {
        $queue[] = $top;
    }
    $left = new Point($node->x - 1, $node->y, $node->depth + 1);
    if ($left->isValid() && $left->isEven() && !in_array($left->toString(), $visited)) {
        $queue[] = $left;
    }
    $right = new Point($node->x + 1, $node->y, $node->depth + 1);
    if ($right->isEven() && !in_array($right->toString(), $visited)) {
        $queue[] = $right;
    }
    $bottom = new Point($node->x, $node->y + 1, $node->depth + 1);
    if ($bottom->isEven() && !in_array($bottom->toString(), $visited)) {
        $queue[] = $bottom;
    }

    handleQueue($queue, $visited, $endX, $endY);
}



//for ($y = 0; $y < $height; $y++) {
//    echo str_pad($y, 2, " ", STR_PAD_LEFT), ' ';
//    for ($x = 0; $x < $width; $x++) {
//
//        if (isEven($x, $y)) {
//            echo '.';
//        }
//        else {
//            echo '#';
//        }
//
//    }
//
//    echo PHP_EOL;
//}