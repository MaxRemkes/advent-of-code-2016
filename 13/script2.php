<?php

ini_set('memory_limit', '6G');

define('FAV', 1358);

require 'Point.php';

$width = 75;
$height = 75;

$favNum = 1358;

$queue = [];

findPoint(1, 1, 31, 39);

function findPoint($startX, $startY, $endX, $endY) {
    $queue[] = new Point($startX, $startY, 0);
    $visited = [];
    handleQueue($queue, $visited, $endX, $endY);
}

function handleQueue($queue, $visited, $endX, $endY) {

    if (count($queue) === 0) {
        return;
    }
    $node = array_shift($queue);

    $nodeString = $node->toString();
    if (!in_array($nodeString, $visited)) {
        $visited[] = $nodeString;
    }

    if ($node->depth == 50) {
        // the highest number is the answer
        echo 'visited: ', count($visited), PHP_EOL;
    }

//    echo $node->toString(), PHP_EOL;

    if ($node->x == $endX && $node->y == $endY) {
        echo '--- ', $node->depth, PHP_EOL;
    }

    $top = new Point($node->x, $node->y - 1, $node->depth + 1);
    if ($top->isValid() && $top->isEven() && !in_array($top->toString(), $visited)) {
        $queue[] = $top;
    }
    $left = new Point($node->x - 1, $node->y, $node->depth + 1);
    if ($left->isValid() && $left->isEven() && !in_array($left->toString(), $visited)) {
        $queue[] = $left;
    }
    $right = new Point($node->x + 1, $node->y, $node->depth + 1);
    if ($right->isEven() && !in_array($right->toString(), $visited)) {
        $queue[] = $right;
    }
    $bottom = new Point($node->x, $node->y + 1, $node->depth + 1);
    if ($bottom->isEven() && !in_array($bottom->toString(), $visited)) {
        $queue[] = $bottom;
    }

    handleQueue($queue, $visited, $endX, $endY);
}