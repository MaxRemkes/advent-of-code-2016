<?php

class Point {


    public $x;
    public $y;
    public $depth;

    function __construct($x, $y, $depth) {
        $this->x = $x;
        $this->y = $y;
        $this->depth = $depth;
    }

    public function toString() {
        return $this->x . ',' . $this->y;
    }

    public function isValid() {
        return $this->x >= 0 && $this->y >= 0;
    }

    public function isEven() {

        $calc = $this->calc();
//        echo $x, ',', $y, ' ', $calc, ' ';
        $sum = $calc + FAV;

        $binary = decbin($sum);
        $count = substr_count($binary, '1');

        return $count % 2 === 0;
    }

    protected function calc() {
        return $this->x * $this->x + 3 * $this->x + 2 * $this->x * $this->y + $this->y + $this->y * $this->y;
    }


}