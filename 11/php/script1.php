<?php

$count = 4;
$moves = 0;
$situation = [];

$situation[0] = ['HM', 'LM'];
$situation[1] = ['HG'];
$situation[2] = ['LG'];
$situation[3] = [];

while (count($situation[3]) !== $count) {
    doMove();
} 

function doMove() {
    
    global $moves;
    global $situation;

    for ($i = 0; $i < count($situation); $i++) {
        if (count($situation[$i]) > 0) {
            $value = array_shift($situation[$i]);
            array_push($situation[$i + 1], $value);
            break;
        }
    }

    var_dump($situation);

    $moves++;
}

echo 'total moves: ', $moves, PHP_EOL;